/**
 * Midgard type for Thorchain endpoint
 */
export declare type ThorchainEndpoint = {
    address?: string;
    chain?: string;
    pubKey?: string;
};
/**
 * Midgard type for Thorchain endpoints
 */
export declare type ThorchainEndpoints = {
    current?: Array<ThorchainEndpoint>;
};
/**
 * IP type
 */
export declare type IP = string;
/**
 * IP list
 */
export declare type IPList = IP[];
/**
 * Internal state for a list of cached IPs
 *
 * Note: It's been exported for running tests only,
 * it will never be exported (public) in `index.ts`
 */
export declare let cachedIpList: IPList;
/**
 * Max. number of pool checks
 *
 * Note: It's been exported for running tests only,
 * it will never be exported (public) in `index.ts`
 */
export declare const MAX_POOL_CHECKS = 5;
/**
 * Getter for `poolChecks` - used by tests only
 */
export declare const getPoolChecks: () => number;
/**
 * Resets `poolChecks` - used by tests only
 */
export declare const resetPoolChecks: () => number;
/**
 * Calculates fault tolerance by a given number
 *
 * @param max - Maximum number to get a fault tolerance from
 *
 * @returns Fault tolerance
 */
export declare const faultTolerance: (max: number) => number;
/**
 * Helper to get endpoint for loading IP list of current active nodes
 *
 * @param isMainnet - Whether to run on `mainnet` (true) or on `testnet` (false) - default: false
 *
 * @returns Seed endpoint
 */
export declare const seedEndpoint: (isMainnet?: boolean) => string;
/**
 * Returns a base url of Midgard API
 *
 * @param ip - IP of a node
 *
 * @returns Midgard base url
 */
export declare const midgartBaseUrl: (ip: string) => string;
/**
 * Returns an endpoint to request pool addresses
 *
 * @param ip - IP of a node
 *
 * @returns Endpoint
 */
export declare const poolAddressEndpoint: (ip: string) => string;
export declare type PoolAddress = Pick<ThorchainEndpoint, 'address' | 'chain'>;
export declare type PoolAddresses = {
    ip: IP;
    addresses: PoolAddress[];
};
export declare type PoolAddressMap = Map<IP, PoolAddress[]>;
/**
 * Helper to compare two `PoolAddress`'es
 *
 * @param a - `PoolAddress`
 * @param b - `PoolAddress`
 *
 * @returns Equity as boolean
 */
export declare const comparePoolAddress: (a: Pick<ThorchainEndpoint, "address" | "chain">, b: Pick<ThorchainEndpoint, "address" | "chain">) => boolean;
/**
 * Helper to compare two lists of `PoolAddress`'es
 *
 * @param a - List of `PoolAddress`
 * @param b - Another list of `PoolAddress`
 *
 * @returns Equity as boolean
 */
export declare const comparePoolAddressLists: (a: Pick<ThorchainEndpoint, "address" | "chain">[], b: Pick<ThorchainEndpoint, "address" | "chain">[]) => boolean;
/**
 * Helper to sort two `PoolAddress`'es by its `chain`
 *
 * @param a - `PoolAddress`
 * @param b - `PoolAddress`
 *
 * @returns Sort number
 */
export declare const sortPoolAddressByChain: (a: Pick<ThorchainEndpoint, "address" | "chain">, b: Pick<ThorchainEndpoint, "address" | "chain">) => 0 | 1 | -1;
/**
 * Helper to transform `ThorchainEndpoint` to `PoolAddress`
 *
 * @param endpoint - `ThorchainEndpoint`
 *
 * @returns `PoolAddress`
 */
export declare const toPoolAddress: ({ address, chain }: ThorchainEndpoint) => Pick<ThorchainEndpoint, "address" | "chain">;
/**
 * Helper to transform a list of `PoolAddresses` to a list of `PoolAddress`
 *
 * @param list - List of `PoolAddresses`
 *
 * @returns List of `PoolAddress`
 */
export declare const toPoolAddressList: (list: PoolAddresses[]) => Pick<ThorchainEndpoint, "address" | "chain">[][];
/**
 * Fetches `PoolAddresses` from Midgard API endpoint
 *
 * @param ip - `ThorchainEndpoint`
 *
 * @returns `PoolAddresses` | Error
 */
export declare const getPoolAddress: (ip: string) => Promise<PoolAddresses>;
/**
 * Fetches list of IPs from seed endpoint
 *
 * @param isMainnet - Whether to run on `mainnet` (true) or on `testnet` (false) - default: false
 *
 * @returns List of nodes (IPs) | Error
 */
export declare const getIpList: (isMainnet?: boolean) => Promise<string[]>;
/**
 * Helper to compare all pool addresses and chains in a list of `PoolAddresses`
 *
 * @param list - List of `PoolAddresses`
 *
 * @returns True if all addresses are equal, false if not
 */
export declare const comparePoolAddresses: (list: PoolAddresses[]) => boolean;
/**
 * Checks nodes about using same pool addresses
 *
 * @param list - List of nodes (IPs)
 *
 * @returns True if all addresses are equal, false if not
 */
export declare const getPoolConsensus: (ipList: IPList) => Promise<IPList>;
/**
 * Returns a random IP from cache
 *
 * @throws Error - if cached list is empty. It should never happen (in theory...)
 */
export declare const cachedBaseUrl: () => string;
/**
 * Key to store timestamp of last check
 */
export declare const TC_BYZANTINE_TIMESTAMP_KEY = "tc-byzantine-ts";
/**
 * Returns current baseUrl of Midgard API of an active and proofed node
 *
 * @param isMainnet - Flag to run `Byzantine` on `mainnet` (true) or on `testnet` (false) - default: false
 * @param noCache - Flag to get baseUrl  from cache or not - default: false
 *
 * @returns Base url of Midgard. It might be a memorized value of a previous (valid) check if available
 */
export declare const baseUrl: (isMainnet?: boolean, noCache?: boolean) => Promise<string>;
