# THORChain Byzantine Module

THORChain Byzantine Module is an anti-phishing module used by `ASGARDEX` clients. It provides an easy way to get a valid, but random base url of [Midgards API](https://gitlab.com/thorchain/midgard/) by proofing active [THORNodes](https://gitlab.com/thorchain/thornode). It will do all the hard work behind the scenes for you.

## Installation

- Install `@thorchain/byzantine-module` from `npm`

```bash
yarn add @thorchain/byzantine-module
```

## Usage

### Basic usage

Whenever your application needs to ask Midgard API for data, call `Byzantine` before. It will return a valid, but random base url of Midgard API provided by an active THORNode. **Please note:** That's the only way to send a transaction to a proofed THORNode at any time.

```js
import byzantine from '@thorchain/byzantine-module'

// baseUrl on testnet
const baseUrl = await byzantine()
// or
// baseUrl on mainnet
const baseUrl = await byzantine(true)

// fetch data from an endpoint, for example to get data of `/v1/pools`
const data = await fetch(`${baseUrl}/v1/pools`)
```

**Side note**: Behind the scenes `Byzantine` is memorizing a proofed list of `baseUrl`s to avoid increasing requests. That's `Byzantine` will recycle its cache every hour only. If you want to get a "fresh", not cached `baseUrl`, set the second parameter to `true`.

```js
// Testnet example:
const baseUrl = await byzantine(false, true)
// Mainnet example:
const baseUrl = await byzantine(true, true)
```

That might be helpful for error handling, see next chapter "Error handling".

### Error handling

In same cases a cached `baseUrl` can be invalid if a node goes offline for any reason. In this case you can force `Byzantine` to return a non-cached `baseUrl` to retry this request, but using another `baseUrl`.

```js
// Get cached `baseUrl` as usual
// Testnet example:
const baseUrl = await byzantine(false)

// Catch errors to retry another request using a "fresh" proofed `baseUrl`
try {
  const data = await fetch(`${baseUrl}/v1/pools`)
  ...
} catch (error) {
  retry()
}

const retry = () => {
  // Get another, "fresh" proofed `baseUrl`
  const anotherBaseUrl = await byzantine(false, true)
  const data = await fetch(`${baseUrl}/v1/pools`)
}

```

**Please note:** Use a non-cached `baseUrl` as few as possible to avoid to increase requests made by `Byzantine`.

## Development

### Build

```bash
yarn build
```

### Tests

```bash
yarn test
```
