# Changelog

## 0.1.1 (2020-04-20)

### Added

- Add `dist` folder (199bcb4798587a175ad944dbba44ed2b957e646f)
- Update consensus to handle < 3 nodes (8c6507492eafeb69790bbb457fd484e71207f992)

### Fixed

- Handle offline nodes properly (7ae1b9d7e108179a38576f3be784afcefd134018)

## 0.1.0 (2020-04-10)

### Initial release
