import { getPoolAddress, poolAddressEndpoint, toPoolAddress, PoolAddresses } from '../src/byzantine'
import { mockThorchainEndpoints, axiosMock, mockThorchainEndpoint } from './mocks'

describe('byzantine', () => {
  afterAll(() => {
    axiosMock.restore()
  })
  describe('getPoolAddress', () => {
    const mockTE1 = mockThorchainEndpoint({ chain: 'C1', address: 'address1' })
    const mockTE2 = mockThorchainEndpoint({ chain: 'C2', address: 'address2' })
    beforeAll(() => {
      axiosMock.onGet(poolAddressEndpoint('121.0.0.1')).reply(
        200,
        mockThorchainEndpoints({
          current: [mockTE1, mockTE2]
        })
      )
      axiosMock.onGet(poolAddressEndpoint('121.0.0.2')).reply(
        200,
        mockThorchainEndpoints({
          current: [mockTE2]
        })
      )
    })
    it('it returns two addresses', async () => {
      const expected: PoolAddresses = {
        ip: '121.0.0.1',
        addresses: [toPoolAddress(mockTE1), toPoolAddress(mockTE2)]
      }
      await expect(getPoolAddress('121.0.0.1')).resolves.toEqual(expected)
    })
    it('it returns one address', async () => {
      const expected: PoolAddresses = {
        ip: '121.0.0.2',
        addresses: [toPoolAddress(mockTE2)]
      }
      await expect(getPoolAddress('121.0.0.2')).resolves.toEqual(expected)
    })
    it('it throws an error if endpoint is not available', async () => {
      axiosMock.onGet(poolAddressEndpoint('121.0.0.10')).timeoutOnce()
      await expect(getPoolAddress('121.0.0.10')).rejects.toThrow()
    })
    it('it throws an error if endpoint is not accessible', async () => {
      axiosMock.onGet(poolAddressEndpoint('121.0.0.10')).reply(500)
      await expect(getPoolAddress('121.0.0.10')).rejects.toThrow()
    })
    it('it throws an error if response does not include valid data', async () => {
      axiosMock.onGet(poolAddressEndpoint('121.0.0.11')).reply(200, mockThorchainEndpoints({ current: undefined }))
      await expect(getPoolAddress('121.0.0.11')).rejects.toThrow()
    })
  })
})
