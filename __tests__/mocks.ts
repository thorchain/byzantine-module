import faker from 'faker'
import { ThorchainEndpoint, ThorchainEndpoints } from '../src/byzantine'

import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import { PoolAddress, toPoolAddress, PoolAddresses } from '../src/byzantine'

export const axiosMock = new MockAdapter(axios)

const defaultThorchainEndpointMock = (): ThorchainEndpoint => ({
  // Note: Following values are not real addresses, pubKeys, chain ids or whatever.
  // It's just about to have some randomness here.
  address: `tbnb${faker.random.uuid()}`,
  chain: faker.finance.currencyCode(),
  pubKey: `thorpub${faker.random.uuid()}`
})

export const mockThorchainEndpoint = (custom?: Partial<ThorchainEndpoint>): ThorchainEndpoint => ({
  ...defaultThorchainEndpointMock(),
  ...custom
})

const defaultThorchainEndpointsMock = (): ThorchainEndpoints => ({
  current: [mockThorchainEndpoint()]
})

export const mockThorchainEndpoints = (custom?: Partial<ThorchainEndpoints>): ThorchainEndpoints => ({
  ...defaultThorchainEndpointsMock(),
  ...custom
})

const defaultPoolAddress = (): PoolAddress => toPoolAddress(mockThorchainEndpoint())

export const mockPoolAddress = (custom?: Partial<PoolAddress>): PoolAddress => ({
  ...defaultPoolAddress(),
  ...custom
})

const defaultPoolAddresses = (): PoolAddresses => ({
  ip: faker.internet.ip(),
  addresses: [mockPoolAddress()]
})

export const mockPoolAddresses = (custom?: Partial<PoolAddresses>): PoolAddresses => ({
  ...defaultPoolAddresses(),
  ...custom
})

describe('mock', () => {
  test('ThorchainEndpoint', () => {
    const result = mockThorchainEndpoint({ chain: 'RUNE' })
    expect(result).not.toBeNull()
    expect(result.chain).toEqual('RUNE')
  })
  test('ThorchainEndpoints', () => {
    const result = mockThorchainEndpoints({ current: [mockThorchainEndpoint(), mockThorchainEndpoint()] })
    expect(result).not.toBeNull()
    expect(result.current?.length).toEqual(2)
  })
})
